Use `npm init` to create a `package.json` file, which will contain information such as keywords, 
project name, test command, and author name.

    npm init

There are two types of dependencies in the `package.json` file: normal dependencies and development dependencies.
You can install normal dependencies using `npm install` (for example, `npm install sass`).

    npm install sass

This will include the dependency in the `package.json` file under "dependencies". These normal dependencies will be
installed on the hosting server when the project is deployed, provided that the server supports Node.js.

You can also install a dependency for development, rather than production, using:

    npm install sass --save-dev
                or 
    npm install sass -D

This will include the dependency in the `package.json` file under "devDependencies". Development dependencies will not
be installed on the hosting server when the project is deployed.

When the dependencies are installed, the `node_modules` folder (where the dependencies will be downloaded) and the
`package-lock.json` (which lists the dependencies' dependencies) file will be created.

The `node_modules` folder can be quite large; therefore, it is not included in repositories, and it can be deleted
when the development stage is finished. Later, it can be restored using `npm install`.

On the other hand, the `package.json` and `package-lock.json` files must not be deleted, as they contain the list of
dependencies.

// You can add snippets to vs code by clicking on file/preferences/configure user snippets and then type the language you want to create the snippet for.
// This will open a json file in which you can write your snippet and the snippet will be usable as soon as the changes to this file are saved.
// To use the snippets, type the prefix on the desired file and hit enter.

// HTML
// Support for light images
"myImages": {
    "prefix" : "im",
    "body": [
        "<picture>",
            "\t<source srcset=\"$1.avif\" type=\"image/avif\">",  // \" => Escape snippet's ""  |  // \t => tab  | $1 = first pointer (input)
            "\t<source srcset=\"$2.webp\" type=\"image/webp\">",
            "\t<img loading=\"lazy\" width=\"200\" height=\"300\" src=\"$3\" alt=\"$4\">",
        "</picture>"
    ]
}

// CSS
"Create Media Querie" : {
		"prefix" : "mq",
		"body" : "@media (min-width $1) {\n    $2\n}"
	}	

// SCSS
	"media query": {
		"prefix": "mq", // When we write "mq" in a scss file, this behavior will apply.
		"body": [
			"@include m.$1{\n\t$2\n}" // This means, write "@include m.", let me write the mixin's name, write "{",                   
		]                             // endline, tab, let me write the code for the mixin, endline, "}"  
	 }

	 "import mixins": {
		"prefix": "imm", // IMportMixins (similar nomenclature to react). Write imm to import mixins
		"body": [
			"@use 'base/mixins' as m;"
		]
	 }
	
	  "import variables": {
		"prefix": "imv", // IMportVariables Write imv to import variables
		"body": [
			"@use 'base/variables' as v;"
		]
	 }
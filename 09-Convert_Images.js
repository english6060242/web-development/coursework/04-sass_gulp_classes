// Require dependencies
const { src, dest, watch, parallel} = require("gulp"); // Here gulp means the installed gulp tool.
const sass = require("gulp-sass")(require("sass"));
const plumber = require('gulp-plumber');
const webp = require('gulp-webp');  /*As admin: npm install --save-dev gulp-webp */  /* import * as webp from 'gulp-webp'; */
const imagemin = require ('gulp-imagemin'); /* npm install --save-dev gulp-imagemin@7.1.0 */
const cache = require ('gulp-cache'); // Required for the lightImages task
const avif = require('gulp-avif');  /* npm install --save-dev gulp-avif */

// Task: Image conversion to webp.
function webpVersion( done ) {
    const options = {
      quality: 50 // 50% quality will still make a good quality image.
    };
    
    src('src/img/**/*.{png,jpg}') // In all folders inside 'src/img', search for png and jpg files
      .pipe( webp(options))       // Convert all images found by src to webp.
      .pipe( dest('build/img'))   // Once the conversion is complete, store the new images in 'build/img'
    done();
  } 
  
  // Task: Convert Non webp images into lighter files.
  function lightImages( done) {
    const options = {
      optimizationLevel: 3 // This will make the images lighter
    }
    src('src/img/**/*.{png,jpg}')     // Find all images
      .pipe(cache(imagemin(options))) // Make images lighter and save them on cache
      .pipe(dest('build/img'))        // Save the lighter images in /build/img
    done();
  }
  
  // Task: Image conversion to avif.
  function avifVersion( done ) {
    const options = {
      quality: 50 // 50% quality will still make a good quality image.
    };
    
    src('src/img/**/*.{png,jpg}') // In all folders inside 'src/img', search for png and jpg files
      .pipe( avif(options))       // Convert all images found by src to webp.
      .pipe( dest('build/img'))   // Once the conversion is complete, store the new images in 'build/img'
    done();
  } 
  
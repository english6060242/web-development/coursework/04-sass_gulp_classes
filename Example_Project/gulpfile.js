// Gulp is a task automation tool
// In this gulpfile we can specify many tasks, such as compiling scss into css, compressing all images given a src
// folder, etc.

function myTask(done) {
    console.log('My first task');
    done(); // Without this callback function, we will get the error: "The following tasks did not complete: firstTask"
}

// Usually we need to invoke the funcitons (myTask();) for the function to be executeed.
// However, in node, this is done differently:

// When we call for "firsTask", myTask() will be executed.
exports.firstTask = myTask; // do not use ()

// There are different ways to call "firsTask". one of them is using npx (which was installed together with node)
// npx gulp firstTask
// In that command, gulp is a binary contained in node_modules

// Another way to call this task is adding a script to package.json unsing npm to run it:
/* "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "sass": "sass --watch src/scss:build/css",
    "firstTask": "gulp firstTask"
  },*/
// Now we can call that task using "npm run firstTask"

// -------------------- Compiling scss with gulp --------------------

const { src, dest, watch} = require("gulp"); // Here gulp means the installed gulp tool.
// It's the one whose version we can find Iin package.json, listed in the devDependencies.
// "require" is a way to import functions from a dependency (from node_modules).
// Here we are using require to gain acces to the src and dest gulp functions. The src function
// allows us to identigy a file or a group of files. The dest fwill help us store files in certain
// destination.
// gulp also has it's own watch so that we don't have to compile every time we make a change to the scss.

const sass = require("gulp-sass")(require("sass")); // Import the "sass" script listed in package.json
// In order to do this we need a conector between sass (which has the correct sintax, compiling information,
// etc) and gulp. This is where the gulp-sass plugin comes in (npm install -D gulp-sass).
// You may find more gulp plugins on gulpjs.com/plugins
// 

function css(done) {
  // Step 1: Find the file for SASS (scss) - Step 2: Compile scss file - Steep 3: Store it
  // We will use pipes to pefrom this steps one after the other.
  //src('src/scss/app.scss').pipe(sass()).pipe(dest("build/css")); // Here a pipe is an action that will be performed after anotherone.
  // In this case the content of the pipe will be executed once src() has concluded. We can also use 
  // multiple pipes: myFunction().pipe().pipe().pipe();
  
  // This can also be written in this format:
  /* src('src/scss/app.scss') */ // Find the scss file for SASS (it retains it in memory for a while and then deletes it unless we do something with it)
  src('src/scss/**/*.scss')  
    .pipe(sass())  //Compile scss file. Here sass() is equal to sass --watch src/scss:build/css from package.json
    .pipe(dest("build/css")); // Store the compiled file in build/css

  done(); // Callback function (this is executed to indicate the task is finished)
}

exports.css = css; // Now the task css (1st) will call the css (2nd) function that contains the pipes
//  Use ---- npx gulp css ---- or add "css": "gulp css" to package.json and use ---- npm run css----

// With only the "Compile the scss code into css" task, it seems that using gulp is too much work
// compared to using the "sass" script in package.json. However, as the amount of tasks increases
// it will be worth it since we gulp will perform several tasks with only one command.

//
function dev(done) {
  /* watch("src/scss/app.scss", css); */ // Watch for changes on the scss file and execute the css function
  // You can chain more functions in this watch.
  watch('src/scss/**/*.scss', css);
  done();
}

exports.dev = dev; // npx gulp dev or add "dev" : "gulp dev" to the scripts in package.json and use npm run dev
// Now this will run the watch, and everytime the scss file is modified, the css task will be executed.
